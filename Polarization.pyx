# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 07:32:28 2016
@author: wallerl & tiagof
Version 2.0
22.03.17 remove fixedHeight() from dockwidget items, added addStretch() in dockwidget-layout instead
28.07.17 added timer routine for non measurement temperature display acttemp()
04.08.17 added reload graph data button
12.10.17 change updateplot to oscilloscopestyle
         change self.x and self.y to predefined x00 000 elements list (saving time, no coping of list)
16.08.19 Change structure to state machine methods 
"""

import sys
import os.path
import pathlib
from threading import Thread
import numpy as np
from PyQt5 import QtCore, QtGui
from PolarizationUI import Ui_MyWidget, Ui_MyDockWidget, Ui_Save
from nonGuiModule import LibModule
import math
from datetime import datetime


class Main(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        # Initialize main window
        self.program_name = "POLARIZATION"
        self.setWindowTitle(self.program_name)

        self.resize(1700, 800)
        # Add file menu
        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)
        self.help_menu.addAction('&Troubleshooting', self.contact)

        # Add sidebar for control
        self._lib = LibModule(self)
        self.myWidget = MyWidget(self)
        self.dockWidget = MyDockWidget(self)
        self.addDockWidget(0x1, self.dockWidget)
        self.setCentralWidget(self.myWidget)

        # Messagebox for error messages
        self.msg = QtGui.QMessageBox()
        self.msg.addButton(QtGui.QPushButton('Cancel'), QtGui.QMessageBox.RejectRole)
        self.msg.addButton(QtGui.QPushButton('OK'), QtGui.QMessageBox.AcceptRole)

        # Connect signals from events to the respective function
        # self.myWidget.vb.sigEvent.connect(self.zoomhome)
        # self.myWidget.vb.sigRect.connect(self.zoom)
        self.dockWidget.sigStart.connect(self.start)
        # self.dockWidget.sigStop.connect(self._lib.stop)
        # self.dockWidget.sigStop.connect(self.stop)
        self.dockWidget.sigNext.connect(self.clickednext)
        self.dockWidget.sigReload.connect(self.reload)
        self.dockWidget.sigSave.connect(self.save)
        self.dockWidget.sigCursor.connect(self.cursor)
        self.dockWidget.sigZoom.connect(self.zoomon)
        self.dockWidget.sigChannel.connect(self.channel)
        self.dockWidget.line1.sigPositionChanged.connect(self.cursormoved)
        self.dockWidget.line2.sigPositionChanged.connect(self.cursormoved)

        # Disable buttons/function for the startup
        self.dockWidget.zoom.setDisabled(True)
        self.dockWidget.cursor.setDisabled(True)
        self.dockWidget.savebt.setDisabled(True)
        self.dockWidget.setDisabled(True)
        self.dockWidget.nextbt.setDisabled(True)

        # Initialize dialog window for saving data to file
        self.saveDialog = MySaveDialog(self)
        self.saveDialog.sigOk.connect(self.savedata)
        self.saveDialog.sigCancel.connect(self.saveDialog.cancel)

        # Initialize dialog window for reloading data from file
        self.reloadDialog = MyReloadDialog(self)

        # Initalize timers for measurements
        self.timerguiupdate = QtCore.QTimer()
        self.timerguiupdate.timeout.connect(self.statemachine)
        # Single Shot timer for ongoing measurements
        self.timeracttemp = QtCore.QTimer()

        self.status = "starting"
        # Variable for data aquisition
        self.datasize = 100  # Maximum should be 360°/5° = 72 steps
        self.x = [0] * self.datasize
        self.y = [0] * self.datasize
        self.xpolar = [0] * self.datasize
        self.ypolar = [0] * self.datasize
        self.count = 0
        self.angle = 0
        self.i = 0
        #Latest voltage value
        self.adata = 0
        self.datatimenow = ""
        # Initial status is all data is saved (No measurements Done)
        self.dataissaved = True

    def fileQuit(self):
        if self.status == "notready":
            self.status = "quitnoserial"
        else:
            self.status = "quit"

    def closeEvent(self, ce):
        self.fileQuit()

    # File menu content
    def about(self):
        QtGui.QMessageBox.about(self, "About",
                                """
Copyright Livia Waller LPC ETHZ 2015
Copyright Tiago Neves LPC ETHZ 2020

""")

    def contact(self):
        QtGui.QMessageBox.about(self, "Troubleshooting",
                                """
- Is the ADC connected to PC?
- Did you choose the right channel?
- Did you choose the right measurement settings?
- Close program, disconnect USB, connect USB again and start program
More trouble with this program? Contact the engineer!
""")

        # the State machine handles the flow of the program, controlled by timer "timerguiupdate"

    def statemachine(self):
        # print("The Status is: ", self.status)
        # if unchanged the next state machine remains the same
        newstatus = self.status
        if self.status == "starting":
            # Disable interface until it's ready
            self.dockWidget.setDisabled(True)
            self.dockWidget.statusmsg.setText("STARTING")
            # start new thread to get serial port number in background
            self.thread = Thread(target=self._lib.findcom)
            # self._lib.findcom()
            self.thread.start()
            newstatus = "connecting"

        elif self.status == "connecting":
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                # If no serial port was found
                if self._lib.ser.port == '1000':
                    # Display error message
                    self.msg.setText('No devices found, please check connections!\n\n Press <OK> to Continue or '
                                     '<Cancel> to Close the application\n')
                    # Show pop-up message and Verify if OK was pressed
                    if self.msg.exec() == True:
                        newstatus = "notready"
                    else:
                        # Close the Program if Cancel is pressed
                        self.close()
                else:
                    self.dockWidget.statusmsg.setText("CONNECTING")
                    # Start new thread to configure arduino in background
                    self.thread = Thread(target=self.acttempstart)
                    self.thread.start()
                    newstatus = "ready"

        elif self.status == "notready":
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                # Enable interface when ready
                self.dockWidget.setDisabled(False)
                self.dockWidget.startbt2.setDisabled(True)
                self.dockWidget.statusmsg.setText("NO DEVICES FOUND ")

        elif self.status == "ready":
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                # self.acttemp()
                self.dockWidget.statusmsg.setText("READY")
                # Enable interface when ready
                self.dockWidget.setDisabled(False)
                newstatus = "readyloop"
                # trigger timer to do a loop measurement when ready
                self.timeracttemp.singleShot(50, self.acttemp)

        elif self.status == "readyloop":
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                self.dockWidget.statusmsg.setText("READY")

        elif self.status == "start_measuring":
            # Disable interface until it's measuring
            # self.dockWidget.setDisabled(True)
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                self.dockWidget.statusmsg.setText("STARTING")
                # If there measurement data unsaved
                if self.dataissaved == False:
                    # Set pop up message
                    self.msg.setText('The Last Measurement is NOT Saved! \n   Start Anyway?')
                    # Show POP-up message and Verify if OK was pressed
                    if self.msg.exec() == True:
                        self.configmeasurement()
                        newstatus = "measuring"
                    else:
                        # Cancel was pressed so keeps in readyloop
                        newstatus = "readyloop"
                else:
                    self.configmeasurement()
                    newstatus = "measuring"

        elif self.status == "measuring":
            self.dockWidget.statusmsg.setText("WAITING FOR INPUT")

        elif self.status == "quit":
            # Stop timer
            self.timeracttemp.stop()
            self.timerguiupdate.stop()
            # Stop serial connection
            self._lib.stop()
            # close the program
            self.close()

        elif self.status == "quitnoserial":
            # Stop timer
            self.timeracttemp.stop()
            self.timerguiupdate.stop()
            # close the program
            self.close()

        else:
            self.dockWidget.statusmsg.setText("UNKNOWN")
        # Update the state machine status
        self.status = newstatus
        # print("The NEW Status is: ", self.status)

    # Function called with Start-Button clicked, starts measurement
    def start(self, *args):
        # setup state machine to start measuring
        self.status = "start_measuring"

    def uncheckbuttons(self):
        # uncheck all toolbar buttons to start
        self.dockWidget.zoom.setAutoExclusive(False);
        self.dockWidget.zoom.setChecked(False);
        self.dockWidget.zoom.setAutoExclusive(True)
        self.dockWidget.cursor.setAutoExclusive(False);
        self.dockWidget.cursor.setChecked(False);
        self.dockWidget.cursor.setAutoExclusive(True)
        self.dockWidget.startbt2.setDisabled(True)
        self.dockWidget.minangle.setDisabled(True)
        self.dockWidget.increment.setDisabled(True)
        self.dockWidget.maxangle.setDisabled(True)
        self.dockWidget.channel.setDisabled(True)
        self.dockWidget.toolbar.setDisabled(True)
        self.dockWidget.nextbt.setDisabled(False)

    # Function called in a new thread to setup a new measurement
    def configmeasurement(self):
        #Update the Window title
        self.setWindowTitle(self.program_name + "              [UNSAVED: --New Measurement--]")
        # Write in INFO section
        self.myWidget.datemsgsaved.setText("UNSAVED --New Measurement--")
        # reset data and acquisiton arrays
        self.uncheckbuttons()
        self.x = [0] * self.datasize
        self.y = [0] * self.datasize
        self.xpolar = [0] * self.datasize
        self.ypolar = [0] * self.datasize
        self.i = 0
        self._lib.xdata = []
        self._lib.wbold = 0.0
        self.count = 0
        self.angle = int(self.dockWidget.minanglebox.value())
        self.dockWidget.nextangle.clear()
        self.dockWidget.nextangle.insert(str(self.angle))
        self.dockWidget.nextbt.setDisabled(False)
        self.myWidget.linearPlotData.setData(x=self.x[0:self.i], y=self.y[0:self.i], clear=True)
        self.myWidget.polarPlotData.setData(x=self.xpolar[0:self.i], y=self.ypolar[0:self.i], clear=True)

    # When NEXT button is clicked the ADC value is recorded ad displayed
    def clickednext(self, *args):
        self.dockWidget.nextbt.setDisabled(True)
        ## READ data from actual angle
        self.x[self.i] = self.angle
        self.y[self.i] = self.adata
        rad = np.deg2rad(self.angle)
        self.xpolar[self.i] = 20 * self.adata * np.cos(rad)
        self.ypolar[self.i] = 20 * self.adata * np.sin(rad)

        self.i += 1
        self.myWidget.linearPlotData.setData(x=self.x[0:self.i], y=self.y[0:self.i], clear=True)
        self.myWidget.polarPlotData.setData(x=self.xpolar[0:self.i], y=self.ypolar[0:self.i], clear=True)
        # time.sleep(0.5)

        self.angle += int(self.dockWidget.incrementbox.currentText())
        if self.angle <= int(self.dockWidget.maxanglebox.value()):
            # if self.count == int(self.dockWidget.maxanglebox.value()):
            # self.dockWidget.nextbt.setText("<< FINISH >>")
            self.dockWidget.nextangle.clear()
            self.dockWidget.nextangle.insert(str(self.angle))
            self.dockWidget.nextbt.setDisabled(False)
        else:
            # self.dockWidget.nextbt.setText('<< Next Value >>')
            self.dockWidget.nextbt.setDisabled(True)
            self.dockWidget.nextangle.clear()
            # Trigger stop measurement
            self.stop()
            # New Unsaved Measurement data available
            self.dataissaved = False
            # Change state machine back to readyloop
            self.status = "readyloop"

    # Function called when measurement is fiinished
    def stop(self, *args):
        self.dockWidget.startbt2.setDisabled(False)
        self.dockWidget.minangle.setDisabled(False)
        self.dockWidget.increment.setDisabled(False)
        self.dockWidget.maxangle.setDisabled(False)
        self.dockWidget.channel.setDisabled(False)
        self.dockWidget.toolbar.setDisabled(False)
        self.dockWidget.zoom.setDisabled(False)
        self.dockWidget.cursor.setDisabled(False)
        self.dockWidget.savebt.setDisabled(False)
        self.dockWidget.nextbt.setDisabled(True)
        # self.status = "starting"

    # Start periodic ADC acquisitions (0.2s)
    def acttempstart(self):
        if self.dockWidget.l.isChecked() == True:
            channel = str(1)
        else:
            channel = str(2)
            # self._lib.startcomacttemp(0.2,self.dockWidget.scalebox.currentText(), channel)
        self._lib.startcomacttemp(0.2, '100', channel)

    # Get new data from Arduino while measuring in between data points
    def acttemp(self):
        if self.status == "readyloop" or self.status == "start_measuring" or self.status == "measuring":
            self.adata = self._lib.acttemp() / 20
            #limits the readings to a maximum of 5Volt, so that the graphs and QProgressBar don't overflow
            if self.adata > 5.0:
                self.adata = 5.0                
            self.dockWidget.acttemp.clear()
            # Translate into Voltage
            self.dockWidget.acttemp.insert(str('{:7.5f}'.format(self.adata)))
            # Show percentage
            self.dockWidget.pbLevel.setValue(int(self.adata * 20))
            # Get and show the Date and Time
            now = datetime.now()
            self.datatimenow = now.strftime("%d/%m/%Y - %H:%M:%S")
            self.myWidget.datemsg.setText("Actual Date and Time: " + self.datatimenow)
            # Trigger single shot for next measurement
            self.timeracttemp.singleShot(150, self.acttemp)

    # Check/uncheck zoom button
    def zoomon(self):
        if self.myWidget.vb.zoom == 1:
            self.myWidget.vb.zoom = 0
        else:
            self.myWidget.vb.zoom = 1

            # Zoom in on plot

    def zoom(self, coords):
        if self.dockWidget.zoom.isChecked() == True:
            points = list(coords)
            self.myWidget.vb.setRange(xRange=(points[0], points[2]), yRange=(points[1], points[3]))

    # Undo zooming on range, reinstall original range of plot
    def zoomhome(self):
        if self.dockWidget.zoom.isChecked() & self.dockWidget.toolbar.isEnabled() == True:
            self.myWidget.vb.setRange(xRange=(min(self.x[:self.i]), max(self.x[:self.i])),
                                      yRange=(min(self.y[:self.i]), max(self.y[:self.i])))

    # Channel selection
    def channel(self):
        # Stop Serial connection
        self._lib.stop()
        # setup state machine to start
        self.status = "starting"

    # Add the cursor lines
    def cursor(self):
        if self.dockWidget.cursor.isChecked() & self.dockWidget.toolbar.isEnabled() == True:
            self.tree = np.asarray(self.x[0:self.i])
            self.dockWidget.line1.setBounds([min(self.x[0:self.i]), max(self.x[0:self.i])])
            self.dockWidget.line2.setBounds([min(self.x[0:self.i]), max(self.x[0:self.i])])
            self.dockWidget.line1.setValue(0)
            self.dockWidget.line2.setValue(0)
            self.myWidget.vb.addItem(self.dockWidget.line1)
            self.myWidget.vb.addItem(self.dockWidget.line2)
            self.myWidget.l.addItem(self.myWidget.label1, 0, 1)
            self.dockWidget.line1.setPos(self.x[int(round(len(self.tree) * 0.2, 0))])
            self.dockWidget.line2.setPos(self.x[int(round(len(self.tree) * 0.7, 0))])
            self.cursormoved()

    # Move the cursor line with mouse drag and display current data point
    def cursormoved(self):
        self.index1 = (np.abs(self.tree - self.dockWidget.line1.value()).argmin())
        self.index2 = (np.abs(self.tree - self.dockWidget.line2.value()).argmin())
        self.dockWidget.line1.setPos(self.x[self.index1])
        self.dockWidget.line2.setPos(self.x[self.index2])
        self.xvalue1 = str('{0:.2f}'.format(self.dockWidget.line1.value()))
        self.yvalue1 = str('{0:.4f}'.format(self.y[self.index1]))
        self.xvalue2 = str('{0:.2f}'.format(self.dockWidget.line2.value()))
        self.yvalue2 = str('{0:.4f}'.format(self.y[self.index2]))
        self.dx = str('{0:.2f}'.format(self.dockWidget.line2.value() - self.dockWidget.line1.value()))
        self.dy = str('{0:.4f}'.format(self.y[self.index2] - self.y[self.index1]))
        self.myWidget.label1.setText(
            'Cursor 1     t:     ' + self.xvalue1 + '    T:    ' + self.yvalue1 + '     Cursor 2  t:     ' + self.xvalue2 + '    T:     ' + self.yvalue2 + '                   dt:     ' + self.dx + '     dT:     ' + self.dy)

        # Call the save window

    def save(self):
        self.saveDialog.execute()

    # Send data to save tp file
    def savedata(self):
        if self.dockWidget.l.isChecked() == True:
            self.channel = str(1)
        else:
            self.channel = str(2)
        #Store the time in a local varible before using it in the data file
        keeptime=self.datatimenow
        #self.myWidget.datemsgsaved.setText("File Saved at: " + keeptime)
        # When save is clicked, it trys to save the file and checks if succeed
        if self.saveDialog.savedata(self.dockWidget.minanglebox.value(), self.dockWidget.maxanglebox.value(),
                                    self.dockWidget.incrementbox.currentText(), self.channel, self.x, self.y, self.i,
                                    self.myWidget, keeptime):
            self.setWindowTitle(self.program_name + "              [SAVED:" + self.saveDialog.filename + "]")
            self.dataissaved = True
        else:
            self.myWidget.datemsgsaved.setText("UNSAVED --New Measurement--")

    # Reload data from file
    def reload(self):
        self.newdatax = []
        self.newdatay = []
        if self.reloadDialog.execute() == True:
            reload_path = os.path.abspath(self.reloadDialog.fname[0])
        else:
            return
        data = []
        with open(reload_path, 'r') as f:
            for l in f:
                data.append(l)
        # find Timestep, Channel und Scale Factor
        indch = [i for i, s in enumerate(data) if "##Channel  " in s]
        indmina = [i for i, s in enumerate(data) if "##Minimum Angle  " in s]
        indmaxa = [i for i, s in enumerate(data) if "##Maximum Angle  " in s]
        indinca = [i for i, s in enumerate(data) if "##Increment Angle  " in s]
        inddate = [i for i, s in enumerate(data) if "##Date:  " in s]
        ch = data[indch[0]][11:]
        ch = ch.rstrip()
        mina = data[indmina[0]][17:]
        maxa = data[indmaxa[0]][17:]
        inca = data[indinca[0]][19:]
        datesaved = data[inddate[0]][9:]

        # set the Min and MAX values
        self.dockWidget.minanglebox.setValue(float(mina))
        self.dockWidget.maxanglebox.setValue(float(maxa))
        #Show saved DATE and TIME, but remove the last character
        self.myWidget.datemsgsaved.setText("File Saved at: " + datesaved[:-1] + " -- Filename: " + reload_path)
        #Set Channel
        if ch == "Left":
            self.dockWidget.l.setChecked(True)
        else:
            self.dockWidget.r.setChecked(True)

        # Check what is the increment value used, and set it in the GUI
        for i in range(5):
            if inca == self.dockWidget.incrementbox.itemText(i):
                self.dockWidget.incrementbox.setCurrentIndex(i)

        # delete all elements with <##>
        inddel = [i for i, s in enumerate(data) if "##" in s]
        for b in reversed(inddel):
            del data[b]
        # split dataelements
        self.i = 0
        for i, elem in enumerate(data):
            nd = data[i].split("  ")
            nd[0] = float(nd[0].rstrip())
            if nd[1] == '':
                nd[1] = np.nan
            else:
                nd[1] = float(nd[1].rstrip())
            self.x[self.i] = nd[0]
            self.y[self.i] = nd[1]
            rad = np.deg2rad(nd[0])
            self.xpolar[self.i] = 20 * nd[1] * np.cos(rad)
            self.ypolar[self.i] = 20 * nd[1] * np.sin(rad)
            self.i += 1
        self.setWindowTitle(self.program_name + "              [FILE OPENED:" + reload_path + "]" )
        self.dataissaved = True
        self.reloadplot()

        # Replot data from file to plot

    def reloadplot(self):
        self.myWidget.linearPlotData.setData(x=self.x[0:self.i], y=self.y[0:self.i], clear=True)
        self.myWidget.polarPlotData.setData(x=self.xpolar[0:self.i], y=self.ypolar[0:self.i], clear=True)
        self.dockWidget.savebt.setDisabled(False)

    # Class for the Widget in the Mainframe


class MyWidget(QtGui.QFrame, Ui_MyWidget):

    def __init__(self, parent=None):
        super(MyWidget, self).__init__(parent)
        self.setupUi(self)


# Sidebar widget, part of MyWidget
class MyDockWidget(QtGui.QDockWidget, Ui_MyDockWidget):
    # Emitted signals of sidebar
    sigStart = QtCore.pyqtSignal()
    sigNext = QtCore.pyqtSignal()
    sigSave = QtCore.pyqtSignal()
    sigCursor = QtCore.pyqtSignal()
    sigZoom = QtCore.pyqtSignal()
    sigReload = QtCore.pyqtSignal()
    sigChannel = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(MyDockWidget, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Setting')
        # Connect sidebar events to signals
        self.startbt2.clicked.connect(self.sigStart)
        self.nextbt.clicked.connect(self.sigNext)
        self.savebt.clicked.connect(self.sigSave)
        self.cursor.clicked.connect(self.sigCursor)
        self.zoom.toggled.connect(self.sigZoom)
        self.reloadbt.clicked.connect(self.sigReload)
        self.l.toggled.connect(self.sigChannel)
        # self.r.toggled.connect(self.sigChannel)


# Window to save data to file
class MySaveDialog(QtGui.QDialog, Ui_Save):
    sigOk = QtCore.pyqtSignal()
    sigCancel = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(MySaveDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Save File')
        self.okbt.clicked.connect(self.sigOk)
        self.cancelbt.clicked.connect(self.sigCancel)

    def execute(self):
        self.exec_()

    # actual saving of data to file, format data for file, add name etc. from save window to file
    def savedata(self, minangle, maxangle, incrementangle, channel, xdata, ydata, arrlength, printgraph, date):
        name = self.line1.text() + '.dat'
        namepicture = self.line1.text() + '.jpg'
        # save_path = os.path.abspath(r'C:\Users\Public') #Mein PC
        #Open Path on DESKTOP
        save_path = os.path.realpath(os.path.expanduser('~/Desktop/LoggerData'))
        pathlib.Path(save_path).mkdir(parents=True, exist_ok=True)  # Make directory if does not exist
        self.filename = os.path.join(save_path, name)
        self.filenamepicture = os.path.join(save_path, namepicture)
        #Verify that the no files with the same exist
        if os.path.isfile(self.filename) == True or os.path.isfile(self.filenamepicture) == True:
            f = QtGui.QFont()
            f.setBold(True)
            d = QtGui.QDialog()
            d.setWindowTitle("Warning")
            d.resize(200, 50)
            ly = QtGui.QVBoxLayout()
            lb = QtGui.QLabel('File with this name already exists!')
            lb.setFont(f)
            ly.addWidget(lb)
            d.setLayout(ly)
            d.exec()
            self.line1.clear()
            self.line2.clear()
            self.line3.clear()
            # self.line4.clear()
            self.line5.clear()
            return False
        else:
            file = open(self.filename, "w")
            file.write('##TITLE  ' + self.line1.text() + '\n')
            file.write('##NAME  ' + self.line2.text() + '\n')
            file.write('##SAMPLE  ' + self.line3.text() + '\n')
            # file.write('##SOLVENT  ' + self.line4.text() + '\n')
            file.write('##COMMENT  ')
            lines = self.line5.toPlainText().split('\n')
            for i in range(len(lines)):
                if i == 0:
                    file.write(lines[i] + '\n')
                else:
                    file.write('##' + lines[i] + '\n')
            if channel == '1':
                file.write('##Channel  ' + 'Left' + '\n')  # Left/right
            if channel == '2':
                file.write('##Channel  ' + 'Right' + '\n')  # Left/right
            file.write('##Minimum Angle  ' + str(minangle) + '\n')
            file.write('##Maximum Angle  ' + str(maxangle) + '\n')
            file.write('##Increment Angle  ' + str(incrementangle) + '\n')
            file.write('##Date:  ' + date + '\n')
            file.write('##' + 'x-data' + '  ' + 'y-data' + '\n')
            for v in range(arrlength):
                if ydata[v] == np.nan:
                    file.write(str("{:5.3f}".format(xdata[v])) + '  ' + 'nan' + '\n')
                else:
                    file.write(str("{:5.3f}".format(xdata[v])) + '  ' + str("{:7.5f}".format(ydata[v])) + '\n')
                # file.write(str("{:5.3f}".format(xdata[v]))+'  '+ydata[v]+'  '+str(ydatastr[v])+'\n')
            file.write('##END##' + '\n')
            file.close()

            printgraph.datemsgsaved.setText("File Saved at: " + date + " -- Filename: " + self.filename)
            ## Renders the Widget into a pixmap
            pixedwidget = printgraph.grab()
            pixedwidget.save(self.filenamepicture, 'jpg')

            self.close()
            self.line1.clear()
            self.line2.clear()
            self.line3.clear()
            # self.line4.clear()
            self.line5.clear()
            return True

    def cancel(self):
        self.close()
        self.isExecuted = 0


# PopUp Window for reloading older data into plot
class MyReloadDialog(QtGui.QFileDialog):

    def __init__(self, parent=None):
        super(MyReloadDialog, self).__init__(parent)
        # reload_path = os.path.abspath(r'C:\Users\Public') #Mein PC
        # reload_path = os.path.abspath(r'C:\Users\pcp\Documents\T_Logger_Data_L') #TreVac
        # reload_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #KAL
        # reload_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #VBW
        reload_path = os.path.realpath(os.path.expanduser('~/Desktop/LoggerData'))
        pathlib.Path(reload_path).mkdir(parents=True, exist_ok=True)  # Make directory if does not exist
        self.setDirectory(reload_path)
        self.setNameFilter("*.dat")
        # self.setFilter("*.dat")
        self.setFileMode(self.ExistingFile)

    def execute(self):
        # self.exec_()
        if self.exec_() == self.Accepted:
            self.fname = self.selectedFiles()
            return True
        else:
            return False


# Main
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv[1:])
    view = Main()
    view.show()
    # stup state machine to start
    view.status = "starting"
    # Default angle increment (45°)
    view.dockWidget.incrementbox.setCurrentIndex(2)
    # start timer to update the GUI
    view.timerguiupdate.start(100)
    sys.exit(app.exec_())
