#!/bin/bash
DIR_I="$HOME/Logger"

install_program () {
  #sudo apt-get update
  #Maybe needed?
  sudo apt-add-repository universe
  sudo apt-get update
  sudo apt-get install apt-transport-https ca-certificates -y
  sudo update-ca-certificates
  #Maybe needed?
  #sudo apt-get -y install software-properties-common
  sudo apt-get -y install python3-pip
  #pip3 install numpy==1.18.5
  pip3 install numpy
  pip3 install pyqt5==5.15.0
  #pip3 install PyQt5-sip
  sudo apt-get -y install pyqt5-dev
  sudo apt-get -y install python3-pyqt5
  sudo apt-get -y install pyqt5-dev-tools
  sudo apt-get -y install qttools5-dev-tools
  pip3 install pyqtgraph==0.12.0
  pip3 install pyserial==3.4
  sudo apt-get -y install git
  git clone https://gitlab.ethz.ch/tiagof/TempLogger.git $DIR_I || git -C $DIR_I pull origin master
  echo "GIT Repository is updated" 
  cp $DIR_I/Polarization.desktop ~/Desktop/
  cp $DIR_I/Cryoscopy.desktop ~/Desktop/
  cp $DIR_I/TempLogger.desktop ~/Desktop/
  cp $DIR_I/TreVac.desktop ~/Desktop/
  echo "Desktop Files copied"
  echo "#!/bin/bash\npython3 ${DIR_I}/Polarization.pyx" | sudo tee /usr/local/bin/Polarization
  echo "#!/bin/bash\npython3 ${DIR_I}/Cryoscopy.pyx" | sudo tee /usr/local/bin/Cryoscopy
  echo "#!/bin/bash\npython3 ${DIR_I}/TempLogger.pyx" | sudo tee /usr/local/bin/TempLogger
  #TreVac program is the same as TempLogger, but 0.2s measurement intervall
  echo "#!/bin/bash\npython3 ${DIR_I}/TempLogger.pyx 0.2" | sudo tee /usr/local/bin/TreVac

  sudo chmod 755 /usr/local/bin/Polarization
  sudo chmod 755 /usr/local/bin/Cryoscopy
  sudo chmod 755 /usr/local/bin/TempLogger
  sudo chmod 755 /usr/local/bin/TreVac

  echo "Binary files Created in: /usr/local/bin"

  sudo usermod -a -G dialout $USER
  echo "Installed Optional Tools" 
  sudo apt-get -y install htop
  install_RStudio

#Local Versions
#pyqt5-dev is already the newest version (5.14.1+dfsg-3build1)
#python3-pip is already the newest version (20.0.2-5ubuntu1)
#Requirement already satisfied: numpy in /home/user/.local/lib/python3.8/site-packages (1.18.5)
#Requirement already satisfied: pyqt5 in /home/user/.local/lib/python3.8/site-packages (5.15.0)
#Requirement already satisfied: PyQt5-sip<13,>=12.8 in /home/user/.local/lib/python3.8/site-packages (from pyqt5) (12.8.0)
#Requirement already satisfied: pyqtgraph in /home/user/.local/lib/python3.8/site-packages (0.11.0)
#Requirement already satisfied: numpy>=1.8.0 in /home/user/.local/lib/python3.8/site-packages (from pyqtgraph) (1.18.5)
#Requirement already satisfied: pyserial in /home/user/.local/lib/python3.8/site-packages (3.4)
}

install_RStudio () {
  sudo apt-get -y install r-base
  #wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2022.07.1-554-amd64.deb
  #sudo dpkg -i rstudio-2022.07.1-554-amd64.deb

  wget https://s3.amazonaws.com/rstudio-ide-build/desktop/jammy/amd64/rstudio-2022.07.2-576-amd64.deb
  #sudo apt deb rstudio-2022.07.2-576-amd64.deb
  sudo dpkg -i rstudio-2022.07.2-576-amd64.deb
  ##If dependencies are missing,this will fix it and install
  sudo apt-get install -y -f
}

self_update_program () {
git -C $DIR_I pull origin master
cp $DIR_I/install.sh $DIR_I/install_self.sh
echo "Copied Self installing script"
echo "RUN the self installing script..."
sh $DIR_I/install_self.sh -f
rm -rf $DIR_I/install_self.sh
echo ":DONE Self Update:"
}

delete_program () {
  pip3 uninstall -y numpy
  pip3 uninstall -y pyqt5
  pip3 uninstall -y pyqtgrap
  pip3 uninstall -y pyserial
  sudo apt-get -y remove pyqt5-dev
  sudo apt-get -y remove python3-pyqt5
  sudo apt-get -y remove pyqt5-dev-tools
  sudo apt-get -y remove qttools5-dev-tools
  sudo apt-get autoremove

  rm -rf $DIR_I

  sudo rm -rf /usr/local/bin/Polarization
  sudo rm -rf /usr/local/bin/Cryoscopy
  sudo rm -rf /usr/local/bin/TempLogger
  sudo rm -rf /usr/local/bin/TreVac

  rm -rf ~/Desktop/Polarization.desktop
  rm -rf ~/Desktop/Cryoscopy.desktop
  rm -rf ~/Desktop/TempLogger.desktop
  rm -rf ~/Desktop/TreVac.desktop
}

show_help () {
  echo "### INSTALL SCRIPT ###"
  echo "run: sh install.sh for default installation (without arguments)"
  echo "### OPTIONAL ARGUMENTS ###"
  echo " -f force install the program"
  echo " -d delete and uninstall program"
  echo " -h help (Show this Help)"
}

#INPUT Parameters?
#echo "INPUT: $1";

if [ "$1" = '-d' ]; then
  ###-d remove and uninstall programs ###
  echo "DELETE PROGRAM FROM: ${DIR_I} Folder."
  delete_program
  echo "ALL REMOVED!"
elif [ "$1" = '-f' ]; then
  ###-f Force install program ###
  echo "Force Installing program in: ${DIR_I} Folder."
  install_program
  echo "PROGRAM INSTALLED, ALL DONE!, Please Reboot!"
elif [ "$1" = '-u' ]; then
  ###-u Update the program ###
  echo "SELF Updating the program in ${DIR_I} Folder"
  self_update_program
  echo "UPDATED, ALL DONE!"
elif [ "$1" = '-h' ]; then
  ###-h Help ###
  show_help
  echo "ALL DONE!"
else
  #NO INPUT Parameters given
  if [ -d "$DIR_I" ]; then
    ###if $DIR exists ###
    echo "SELF Updating the program in ${DIR_I} Folder"
    self_update_program
    #install_program
    echo "UPDATED, ALL DONE!"
  else
    ###if $DIR does NOT exist ###
    echo "Installing program in: ${DIR_I} Folder."
    install_program
    echo "PROGRAM INSTALLED, ALL DONE!, Please Reboot!"
  fi
fi

