## Introduction
TempLogger is a set of Python applications to visualize and record Data for different experiments.

The Data is obtained using an ADC that connects to the computer with USB.
The ADC Hardware communicates with serial port commands, and the project is available at:
https://gitlab.ethz.ch/tiagof/Arduino_High_Resolution_ADC

### Software Dependencies
This application uses:
- Python3
- pyqt5
- numpy
- pyqtgraph
- pyserial
### Hardware Dependencies
Computer with USB port.
This application uses the custom built hardware "High Resolution ADC (23Bit+Sign)".
This is an ADC (ADS1248) board connected to an Arduino Uno Platform.

## Linux support 
- Mint 21 (cinnamon) 
- Kubuntu 22.04
- Xubuntu 20.04

### Software installation and update
Run the script:
```
$sh install.sh
```
### Allow the program to access the Serial Port

When the board is connected to an USB port, the driver will be automatically loaded and a port like "/dev/ttyACM0" will be asset.
The application will search and access this Serial Port.
But the user has to belong to GROUP "dialout" (Serial port group) so no root rights are needed to run the application.

In the script "sh install.sh" there is the instruction: "$sudo usermod -a -G dialout $USER" doing that.

### Developpers
```
$sudo apt-get install snapd
$sudo snap install pycharm-community --classic
```
