﻿# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 07:35:54 2016

@author: wallerl
User interface graphical items, consist of MyWidget, DockWidget, ViewBox and Save-Window
MyWidget:
- Main frame containing the Viewbox and the DockWidget

DockWidget:
-Sidebar with all settings, buttons
ViewBox:
-Graph for plotting live temperature measurement 
Save-Window:
-Pop-Up Window for saving/reloading data
Event-Signals
-Signals connect actions (buttons,parameter,click, drag etc.) to the corresponding function

"""

from PyQt5 import QtCore, QtGui, Qt
from pyqtgraph.Point import Point
import pyqtgraph as pg
import numpy as np


class Ui_MyWidget(object):
    def setupUi(self, widget):
        self.verticalLayout = QtGui.QVBoxLayout(widget)

        self.date = QtGui.QGroupBox("INFO:")
        # self.date = QtGui.QGroupBox()
        self.datelayout = QtGui.QVBoxLayout()
        self.datemsg = QtGui.QLabel()
        self.datemsg.setText("")
        self.datemsgsaved = QtGui.QLabel()
        self.datemsgsaved.setText("")
        self.datelayout.addWidget(self.datemsg)
        self.datelayout.addWidget(self.datemsgsaved)
        self.date.setLayout(self.datelayout)
        self.verticalLayout.addWidget(self.date)
        self.frame = QtGui.QFrame(widget)
        # self.frame.setFrameShape(QtGui.QFrame.NoFrame)
        # self.frame.setFrameShadow(QtGui.QFrame.Plain)
        # self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
        self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
        self.verticalLayout.addWidget(self.frame)

        self.label = QtGui.QLabel("Plot1")
        # self.horizontalLayout.addWidget(self.label)
        # self.linearPlot = pg.PlotWidget()
        self.linearPlot = pg.PlotWidget(title="Linear View --Signal Level [Volt]--",
                                        labels={'left': 'Signal Level [Volt]', 'bottom': 'Angle [°]'})
        self.linearPlot.showGrid(True, True, 30)
        # self.linearPlot.getAxis('left').enableAutoSIPrefix(enable=False)
        # Create Labels lines every 1Volt with small lines every 0.2Volt
        self.linearPlot.getAxis('left').setTickSpacing(1, 0.2)
        # Create Labels lines every 90° with small lines every 30°
        self.linearPlot.getAxis('bottom').setTickSpacing(90, 30)
        self.linearPlot.setMouseEnabled(x=False, y=False)
        self.linearPlot.setLimits(xMin=-1, xMax=370, yMin=0, yMax=5.1)
        self.linearPlot.setRange(rect=None, xRange=(0, 360), yRange=(0, 5), padding=None, update=True,
                                 disableAutoRange=True)
        # self.linearPlot.setXRange(0, 360)
        # self.linearPlot.setYRange(0, 5)
        self.linearPlotData = self.linearPlot.plot(symbol='o')
        self.linearPlotData.setPen(pg.mkPen(color='y', width=2))
        self.horizontalLayout.addWidget(self.linearPlot)

        self.polarPlot = pg.PlotWidget(title="Polar View --Signal Level [%]--")
        self.polarPlot.hideAxis('bottom')
        self.polarPlot.hideAxis('left')
        self.polarPlot.setAspectLocked()
        self.polarPlot.setMouseEnabled(x=False, y=False)

        # Add polar grid lines
        for r in range(0, 101, 20):
            circle = pg.QtGui.QGraphicsEllipseItem(-r, -r, r * 2, r * 2)
            # circle.setPen(pg.mkPen(color='b', width=1))
            circle.setPen(pg.mkPen(color=(0, 50 + 2 * r, 50 + 2 * r), width=3))
            self.polarPlot.addItem(circle)

            legendlevel = pg.TextItem(text=str(r) + '%', color=(0, 100 + r, 100 + r))
            legendlevel.setPos(2, r + 8)
            self.polarPlot.addItem(legendlevel)

        for angle in range(0, 360, 30):
            rad = np.deg2rad(angle)
            line = pg.PlotCurveItem([0, 100 * np.cos(rad)], [0, 100 * np.sin(rad)])
            line.setPen(pg.mkPen(color='w', width=1))
            self.polarPlot.addItem(line)

            legendangle = pg.TextItem(text=str(angle) + '°', color='w')
            legendangle.setPos(108 * np.cos(rad) - 7, 108 * np.sin(rad) + 5)
            self.polarPlot.addItem(legendangle)

        self.polarPlotData = self.polarPlot.plot(symbol='o')
        self.polarPlotData.setPen(pg.mkPen(color='y', width=2))

        self.horizontalLayout.addWidget(self.polarPlot)


class Ui_MyDockWidget(object):

    def setupUi(self, obj):
        # Settings
        obj.setFeatures(QtGui.QDockWidget.DockWidgetFloatable)
        # obj.dock_widget.setFeatures(QtGui.QDockWidget.DockWidgetMovable)

        # Buttons
        obj.startbt2 = QtGui.QPushButton('Start new measurement', obj)
        obj.nextbt = QtGui.QPushButton('<< SAVE VALUE >>', obj)

        # signal Level display
        obj.acttempbox2 = QtGui.QGroupBox("Signal Level [%]")
        obj.acttemplayout2 = QtGui.QHBoxLayout()
        obj.pbLevel = QtGui.QProgressBar()
        obj.pbLevel.setMaximum(100)
        obj.pbLevel.setProperty("value %", 123)
        obj.pbLevel.setTextVisible(True)
        obj.pbLevel.setOrientation(QtCore.Qt.Horizontal)
        obj.acttemplayout2.addWidget(obj.pbLevel)
        # self.acttemplayout.addStretch()
        obj.acttempbox2.setLayout(obj.acttemplayout2)

        obj.acttempbox = QtGui.QGroupBox("Signal Level [Volt]")
        obj.acttemplayout = QtGui.QVBoxLayout()
        obj.acttemp = QtGui.QLineEdit(obj)
        obj.acttemp.setReadOnly(True)
        obj.acttemplayout.addWidget(obj.acttemp)
        obj.acttempbox.setLayout(obj.acttemplayout)

        # Increment
        obj.increment = QtGui.QGroupBox("Choose Increment Angle [Degrees]")
        # obj.increment.setFixedHeight(50)
        obj.incrementlayout = QtGui.QVBoxLayout()
        obj.incrementbox = QtGui.QComboBox()
        obj.incrementbox.addItem("5")
        obj.incrementbox.addItem("10")
        obj.incrementbox.addItem("15")
        obj.incrementbox.addItem("30")
        obj.incrementbox.addItem("40")
        obj.incrementbox.addItem("45")
        obj.incrementlayout.addWidget(obj.incrementbox)
        obj.increment.setLayout(obj.incrementlayout)

        # max measurement time
        obj.maxmtimebox = QtGui.QGroupBox("Max. Measurement Time")
        obj.maxmtimelayout = QtGui.QVBoxLayout()
        obj.maxmtime = QtGui.QLineEdit(obj)
        obj.maxmtime.setReadOnly(True)
        obj.maxmtimelayout.addWidget(obj.maxmtime)
        obj.maxmtimebox.setLayout(obj.maxmtimelayout)

        # Minumum start Angle
        obj.minangle = QtGui.QGroupBox("Minimum Angle")
        # obj.minangle.setFixedHeight(50)
        obj.minanglelayout = QtGui.QVBoxLayout()
        obj.minanglebox = QtGui.QDoubleSpinBox()
        obj.minanglebox.setButtonSymbols(obj.minanglebox.UpDownArrows)
        obj.minanglebox.setMaximum(359)
        obj.minanglebox.setMinimum(0)
        obj.minanglebox.setSingleStep(1)
        obj.minanglebox.setValue(0)
        obj.minanglelayout.addWidget(obj.minanglebox)
        obj.minangle.setLayout(obj.minanglelayout)

        # Maximumend Angle
        obj.maxangle = QtGui.QGroupBox("Maximum Angle")
        # obj.maxangle.setFixedHeight(50)
        obj.maxanglelayout = QtGui.QVBoxLayout()
        obj.maxanglebox = QtGui.QDoubleSpinBox()
        obj.maxanglebox.setButtonSymbols(obj.maxanglebox.UpDownArrows)
        obj.maxanglebox.setMaximum(360)
        obj.maxanglebox.setMinimum(1)
        obj.maxanglebox.setSingleStep(1)
        obj.maxanglebox.setValue(360)
        obj.maxanglelayout.addWidget(obj.maxanglebox)
        obj.maxangle.setLayout(obj.maxanglelayout)

        # Show angle
        obj.showangle = QtGui.QGroupBox("Set Polarizator angle to:")
        # obj.channel.setFixedHeight(70)
        obj.showanglelayout = QtGui.QVBoxLayout()
        obj.nextangle = QtGui.QLineEdit(obj)
        obj.nextangle.setReadOnly(True)
        obj.showanglelayout.addWidget(obj.nextangle)
        obj.showangle.setLayout(obj.showanglelayout)

        # Channel
        obj.channel = QtGui.QGroupBox("Choose channel")
        # obj.channel.setFixedHeight(70)
        obj.channellayout = QtGui.QVBoxLayout()
        obj.l = QtGui.QRadioButton('Left')
        obj.r = QtGui.QRadioButton('Right')
        obj.l.setChecked(True)
        obj.channellayout.addWidget(obj.l)
        obj.channellayout.addWidget(obj.r)
        obj.channel.setLayout(obj.channellayout)

        # Toolbar
        obj.toolbar = QtGui.QGroupBox("Toolbar")
        # obj.toolbar.setFixedHeight(120)
        obj.toolbarlayout = QtGui.QVBoxLayout()
        obj.savebt = QtGui.QPushButton('Save data')
        obj.toolbarlayout.addWidget(obj.savebt)
        obj.reloadbt = QtGui.QPushButton('Reload data')
        obj.toolbarlayout.addWidget(obj.reloadbt)
        obj.zoom = QtGui.QRadioButton('Zoom +/-')
        # obj.toolbarlayout.addWidget(obj.zoom)
        obj.cursor = QtGui.QRadioButton('Cursor')
        # obj.toolbarlayout.addWidget(obj.cursor)
        obj.toolbar.setLayout(obj.toolbarlayout)

        obj.status = QtGui.QGroupBox("Status")
        obj.statuslayout = QtGui.QVBoxLayout()
        obj.statusmsg = QtGui.QLabel()
        obj.statusmsg.setText("STARTING")
        obj.statuslayout.addWidget(obj.statusmsg)
        obj.status.setLayout(obj.statuslayout)

        # Cursor lines
        obj.line1 = pg.InfiniteLine(angle=90, movable=True)
        obj.line2 = pg.InfiniteLine(angle=90, movable=True)

        # Add all items to the sidebar
        obj.docky = QtGui.QWidget(obj)
        # obj.docky.setFixedHeight(450)
        obj.dlayout = QtGui.QVBoxLayout()
        obj.docky.setLayout(obj.dlayout)
        obj.dlayout.addWidget(obj.startbt2)
        obj.dlayout.addWidget(obj.showangle)
        obj.dlayout.addWidget(obj.nextbt)
        obj.dlayout.addWidget(obj.acttempbox2)
        obj.dlayout.addWidget(obj.acttempbox)
        obj.dlayout.addWidget(obj.increment)
        obj.dlayout.addWidget(obj.minangle)
        obj.dlayout.addWidget(obj.maxangle)
        # obj.dlayout.addWidget(obj.maxmtimebox)
        # obj.dlayout.addWidget(obj.channel)
        obj.dlayout.addWidget(obj.toolbar)
        obj.dlayout.addWidget(obj.status)
        obj.dlayout.addStretch()
        obj.setWidget(obj.docky)

    # Template for graphical contents of save window


class Ui_Save(object):

    def setupUi(self, obj):
        obj.dialog = QtGui.QDialog()
        obj.layout = QtGui.QVBoxLayout()
        obj.dialog.setLayout(obj.layout)

        obj.title = QtGui.QLabel("Save data in LoggerData\n")
        obj.font = QtGui.QFont()
        obj.font.setBold(True)
        obj.font.setWeight(75)
        obj.title.setFont(obj.font)
        obj.title1 = QtGui.QLabel("Filename (without extension <.dat>)")
        obj.line1 = QtGui.QLineEdit()
        obj.title2 = QtGui.QLabel("Your Name")
        obj.line2 = QtGui.QLineEdit()
        obj.title3 = QtGui.QLabel("Sample")
        obj.line3 = QtGui.QLineEdit()
        # obj.title4 = QtGui.QLabel("Solvent")
        # obj.line4 = QtGui.QLineEdit()
        obj.title5 = QtGui.QLabel("Comment")
        obj.line5 = QtGui.QTextEdit()
        obj.title6 = QtGui.QLabel("\nNOTE: An Image file <.jpg> will also be generated\n")

        obj.btlayout = QtGui.QHBoxLayout()
        obj.okbt = QtGui.QPushButton('Ok', obj)
        obj.cancelbt = QtGui.QPushButton('Cancel', obj)
        obj.btlayout.addWidget(obj.okbt)
        obj.btlayout.addWidget(obj.cancelbt)

        obj.layout.addWidget(obj.title)
        obj.layout.addWidget(obj.title1)
        obj.layout.addWidget(obj.line1)
        obj.layout.addWidget(obj.title2)
        obj.layout.addWidget(obj.line2)
        obj.layout.addWidget(obj.title3)
        obj.layout.addWidget(obj.line3)
        # obj.layout.addWidget(obj.title4)
        # obj.layout.addWidget(obj.line4)
        obj.layout.addWidget(obj.title5)
        obj.layout.addWidget(obj.line5)
        obj.layout.addWidget(obj.title6)
        obj.layout.addLayout(obj.btlayout)
        obj.setLayout(obj.layout)
