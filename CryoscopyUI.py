﻿# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 07:35:54 2016

@author: wallerl
User interface graphical items, consist of MyWidget, DockWidget, ViewBox and Save-Window
MyWidget:
- Main frame containing the Viewbox and the DockWidget

DockWidget:
-Sidebar with all settings, buttons
ViewBox:
-Graph for plotting live temperature measurement 
Save-Window:
-Pop-Up Window for saving/reloading data
Event-Signals
-Signals connect actions (buttons,parameter,click, drag etc.) to the corresponding function

"""

from PyQt5 import QtCore, QtGui, Qt
from pyqtgraph.Point import Point
import pyqtgraph as pg

class Ui_MyWidget(object):
    sigMouseMove = QtCore.pyqtSignal()
    
    def setupUi(self, obj):
        obj.layout = QtGui.QVBoxLayout(obj)   
 		#Plot with Viewbox
        obj.widget = pg.GraphicsView()
        obj.l = QtGui.QGraphicsGridLayout()
        obj.l.setHorizontalSpacing(0)
        obj.l.setVerticalSpacing(0)
        obj.vb = ViewBox(enableMenu = False)
        obj.l.addItem(obj.vb,1,1)
        #Actual plot item
        obj.linewidth = pg.mkPen(width = 2)
        obj.p1 = pg.PlotDataItem(pen = obj.linewidth)
        obj.vb.addItem(obj.p1)
        #Add horizontal infinite line (for trigger)
        obj.ap2=pg.TextItem()
        obj.p2 = pg.InfiniteLine(angle=0, pen='r', label='Trigger', labelOpts={'position':0.04})
        obj.vb.addItem(obj.p2)
        #Add plot settings/axis names
        obj.xScale = pg.AxisItem(orientation='bottom', linkView=obj.vb)
        obj.l.addItem(obj.xScale, 2, 1)
        obj.yScale = pg.AxisItem(orientation='left', linkView=obj.vb)
        obj.l.addItem(obj.yScale, 1, 0)
        obj.xScale.setLabel("Time", units="s")
        obj.yScale.setLabel('Resistance', units='kohm')
        obj.xScale.enableAutoSIPrefix(False)
        obj.yScale.enableAutoSIPrefix(False)
        
        obj.label1 = pg.LabelItem()
        obj.l.addItem(obj.label1, 0,1)
        obj.vb.setMouseEnabled(x=False,y = False)
        obj.vb.setLimits(xMin = 0)
        obj.vb.enableAutoRange(axis =ViewBox.XAxis)
        obj.widget.centralWidget.setLayout(obj.l)
        obj.layout.addWidget(obj.widget)
            
class ViewBox(pg.ViewBox):
    sigEvent = QtCore.pyqtSignal()
    sigRect = QtCore.pyqtSignal(tuple)

    def __init__(self, *args, **kwds):
        pg.ViewBox.__init__(self, *args, **kwds)
        self.zoom = 0
    
    #overwrite pyqtgraph function for zoom with right mouse drag, zoom out with left mouse click
    def mouseDragEvent(self,ev): 
        if self.zoom == 1:
            if ev.button() == QtCore.Qt.RightButton:
                ev.ignore()    
            else:
                pg.ViewBox.mouseDragEvent(self, ev)
                
            ev.accept()
            pos = ev.pos()
            
            if ev.button() == QtCore.Qt.LeftButton:
                print('drag')
                if ev.isFinish():
                    self.rbScaleBox.hide()
                    self.ax = QtCore.QRectF(Point(ev.buttonDownPos(ev.button())), Point(pos))
                    self.ax = self.childGroup.mapRectFromParent(self.ax) 
                    self.Coords =  self.ax.getCoords()
                    self.Coords = (round(self.Coords[0],4),round(self.Coords[1],4),round(self.Coords[2],4),round(self.Coords[3],4)) #Zoomgenauigkeit begrenzen
                    print(self.Coords)
                    self.sigRect.emit(self.Coords)
                    print('finished')
                    
                else:
                    self.updateScaleBox(ev.buttonDownPos(), ev.pos())

    #overwrite pyqtgraph function, to emit signal 
    def mouseClickEvent(self,ev):         
        if ev.button() == QtCore.Qt.RightButton:
            self.sigEvent.emit()  

#Template class for graphical contents of sidebar
class Ui_MyDockWidget(object):

    def setupUi(self, obj):
                
        #Settings
        obj.setFeatures(QtGui.QDockWidget.DockWidgetFloatable)
        #obj.dock_widget.setFeatures(QtGui.QDockWidget.DockWidgetMovable)
        
        #Buttons
        obj.startbt2 = QtGui.QPushButton('Start new measurement', obj)
        obj.stopbt2 = QtGui.QPushButton('Stop measuring', obj)

        #temperature display
        obj.acttempbox = QtGui.QGroupBox("Resistance [kohm]")
        obj.acttemplayout =QtGui.QVBoxLayout()
        obj.acttemp = QtGui.QLineEdit( obj)
        obj.acttemp.setReadOnly(True)
        obj.acttemplayout.addWidget(obj.acttemp)
        obj.acttempbox.setLayout(obj.acttemplayout)

        #max measurement time
        obj.maxmtimebox = QtGui.QGroupBox("Max. Measurement Time")
        obj.maxmtimelayout =QtGui.QVBoxLayout()
        obj.maxmtime = QtGui.QLineEdit( obj)
        obj.maxmtime.setReadOnly(True)
        obj.maxmtimelayout.addWidget(obj.maxmtime)
        obj.maxmtimebox.setLayout(obj.maxmtimelayout)

        #max measurement time
        obj.triggerresistancebox = QtGui.QGroupBox("Trigger Resitance value (T=-8°C)")
        obj.triggerresistancelayout =QtGui.QVBoxLayout()
        obj.triggerresistance = QtGui.QDoubleSpinBox()
        obj.triggerresistance.setReadOnly(True)
        obj.triggerresistance.setSuffix(" [kohm]")
        obj.triggerresistance.setButtonSymbols(obj.triggerresistance.NoButtons)
        obj.triggerresistancelayout.addWidget(obj.triggerresistance)
        obj.triggerresistancebox.setLayout(obj.triggerresistancelayout)

        #Measurement intervall
        obj.intervall = QtGui.QGroupBox("Choose measure intervall [s]")
        #obj.intervall.setFixedHeight(50)
        obj.intervalllayout =QtGui.QVBoxLayout()
        obj.intervallbox = QtGui.QDoubleSpinBox()
        obj.intervallbox.setButtonSymbols(obj.intervallbox.UpDownArrows)
        obj.intervallbox.setMaximum(4.0)
        obj.intervallbox.setMinimum(0.01)
        obj.intervallbox.setSingleStep(0.01)
        obj.intervallbox.setValue(0.50)  
        obj.intervalllayout.addWidget(obj.intervallbox)
        obj.intervall.setLayout(obj.intervalllayout)
        
        #Scale 
        obj.scale = QtGui.QGroupBox("Full Scale[°C]")
        #obj.scale.setFixedHeight(50)
        obj.scalelayout =QtGui.QVBoxLayout()
        obj.scalebox = QtGui.QComboBox()
        obj.scalebox.addItem('50')
        #obj.scalebox.addItem('25')
        obj.scalebox.addItem('100')
        obj.scalebox.addItem('200')
        obj.scalelayout.addWidget(obj.scalebox)
        obj.scale.setLayout(obj.scalelayout)
        
        #Channel
        obj.channel = QtGui.QGroupBox("Choose channel")
        #obj.channel.setFixedHeight(70)
        obj.channellayout =QtGui.QVBoxLayout()
        obj.l = QtGui.QRadioButton('Left')
        obj.r = QtGui.QRadioButton('Right')
        obj.l.setChecked(True)  
        obj.channellayout.addWidget(obj.l)
        obj.channellayout.addWidget(obj.r)
        obj.channel.setLayout(obj.channellayout)
        
        #Toolbar
        obj.toolbar = QtGui.QGroupBox("Toolbar")
        #obj.toolbar.setFixedHeight(120)
        obj.toolbarlayout =QtGui.QVBoxLayout()
        obj.savebt = QtGui.QPushButton('Save data')
        obj.toolbarlayout.addWidget(obj.savebt)
        obj.reloadbt = QtGui.QPushButton('Reload data')
        obj.toolbarlayout.addWidget(obj.reloadbt)
        obj.zoom = QtGui.QRadioButton('Zoom +/-')
        obj.toolbarlayout.addWidget(obj.zoom)
        obj.cursor = QtGui.QRadioButton('Cursor')
        obj.toolbarlayout.addWidget(obj.cursor)
        obj.toolbar.setLayout(obj.toolbarlayout)
        
        obj.status = QtGui.QGroupBox("Status")
        obj.statuslayout =QtGui.QVBoxLayout()
        obj.statusmsg = QtGui.QLabel()
        obj.statusmsg.setText("STARTING")
        obj.statuslayout.addWidget(obj.statusmsg)  
        obj.status.setLayout(obj.statuslayout)

        #Cursor lines
        obj.line1 = pg.InfiniteLine( angle=90, movable=True)
        obj.line2 = pg.InfiniteLine( angle=90, movable=True)
        
        #Add all items to the sidebar
        obj.docky =QtGui.QWidget(obj)
       # obj.docky.setFixedHeight(450)
        obj.dlayout = QtGui.QVBoxLayout()
        obj.docky.setLayout(obj.dlayout)
        obj.dlayout.addWidget(obj.startbt2) 
        obj.dlayout.addWidget(obj.stopbt2)
        obj.dlayout.addWidget(obj.acttempbox)
        obj.dlayout.addWidget(obj.intervall)
        obj.dlayout.addWidget(obj.maxmtimebox)
        obj.dlayout.addWidget(obj.triggerresistancebox)
        #obj.dlayout.addWidget(obj.scale)
        #obj.dlayout.addWidget(obj.channel)
        obj.dlayout.addWidget(obj.toolbar) 
        obj.dlayout.addWidget(obj.status)
        obj.dlayout.addStretch()  
        obj.setWidget(obj.docky)  
 
#Template for graphical contents of save window        
class Ui_Save(object):
    
    def setupUi(self, obj):     
        obj.dialog = QtGui.QDialog()
        obj.layout = QtGui.QVBoxLayout()
        obj.dialog.setLayout(obj.layout)
        
        obj.title = QtGui.QLabel("Save data in LoggerData\n") 
        obj.font = QtGui.QFont()
        obj.font.setBold(True)
        obj.font.setWeight(75)
        obj.title.setFont(obj.font)       
        obj.title1 = QtGui.QLabel("Filename (without extension.dat)")        
        obj.line1 = QtGui.QLineEdit()
        obj.title2 = QtGui.QLabel("Your Name")        
        obj.line2 = QtGui.QLineEdit()
        obj.title3 = QtGui.QLabel("Sample")        
        obj.line3 = QtGui.QLineEdit()
        obj.title4 = QtGui.QLabel("Solvent")        
        obj.line4 = QtGui.QLineEdit()
        obj.title5 = QtGui.QLabel("Comment")
        obj.line5 = QtGui.QTextEdit()
        obj.title6 = QtGui.QLabel("\nNOTE: An Image file <.jpg> will also be generated\n")
        
        obj.btlayout = QtGui.QHBoxLayout()
        obj.okbt = QtGui.QPushButton('Ok', obj)        
        obj.cancelbt = QtGui.QPushButton('Cancel', obj)
        obj.btlayout.addWidget(obj.okbt)        
        obj.btlayout.addWidget(obj.cancelbt) 
        
        obj.layout.addWidget(obj.title)
        obj.layout.addWidget(obj.title1)        
        obj.layout.addWidget(obj.line1)
        obj.layout.addWidget(obj.title2)
        obj.layout.addWidget(obj.line2)
        obj.layout.addWidget(obj.title3)
        obj.layout.addWidget(obj.line3)
        obj.layout.addWidget(obj.title4)
        obj.layout.addWidget(obj.line4)
        obj.layout.addWidget(obj.title5)
        obj.layout.addWidget(obj.line5)
        obj.layout.addWidget(obj.title6)
        obj.layout.addLayout(obj.btlayout)
        obj.setLayout(obj.layout)        
        
