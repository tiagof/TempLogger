# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 07:32:28 2016
@author: wallerl & tiagof
Version 2.0
22.03.17 remove fixedHeight() from dockwidget items, added addStretch() in dockwidget-layout instead
28.07.17 added timer routine for non measurement temperature display acttemp()
04.08.17 added reload graph data button
12.10.17 change updateplot to oscilloscopestyle
         change self.x and self.y to predefined x00 000 elements list (saving time, no coping of list)
16.08.19 Change structure to state machine methods 
"""

import sys
import os.path
import pathlib
from threading import Thread
import numpy as np
from PyQt5 import QtCore, QtGui
from TempLoggerUI import Ui_MyWidget, Ui_MyDockWidget, Ui_Save
from nonGuiModule import LibModule
import math
import time

class Main(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        #Initialize main window
        self.setWindowTitle("Temperature Logger")

        self.resize(1000,600)
        #Add file menu 
        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)
        self.help_menu.addAction('&Troubleshooting', self.contact)

        #Add sidebar for control
        self._lib = LibModule(self)
        self.myWidget = MyWidget(self)
        self.dockWidget = MyDockWidget(self)
        self.addDockWidget(0x1, self.dockWidget)
        self.setCentralWidget(self.myWidget)

        #Messagebox for error messages
        self.msg = QtGui.QMessageBox()
        #self.msg.addButton(QtGui.QPushButton('Close'),QtGui.QMessageBox.RejectRole)
        self.msg.addButton(QtGui.QPushButton('OK'),QtGui.QMessageBox.AcceptRole)
        

        #Connect signals from events to the respective function
        self.myWidget.vb.sigEvent.connect(self.zoomhome)
        self.myWidget.vb.sigRect.connect(self.zoom)
        self.dockWidget.sigStart.connect(self.start)
        self.dockWidget.sigStop.connect(self._lib.stop)
        self.dockWidget.sigStop.connect(self.stop)
        self.dockWidget.sigReload.connect(self.reload)
        self.dockWidget.sigSave.connect(self.save)
        self.dockWidget.sigCursor.connect(self.cursor)
        self.dockWidget.sigZoom.connect(self.zoomon)
        self.dockWidget.sigChannel.connect(self.channel)
        self.dockWidget.line1.sigPositionChanged.connect(self.cursormoved)
        self.dockWidget.line2.sigPositionChanged.connect(self.cursormoved)
        self.dockWidget.intervallbox.valueChanged.connect(self.maxtime)

        #Disable buttons/function for the startup
        self.dockWidget.zoom.setDisabled(True)
        self.dockWidget.cursor.setDisabled(True)
        self.dockWidget.savebt.setDisabled(True)
        self.dockWidget.setDisabled(True)
        self.dockWidget.stopbt2.setDisabled(True)

        #Initialize dialog window for saving data to file
        self.saveDialog = MySaveDialog(self)
        self.saveDialog.sigOk.connect(self.savedata)
        self.saveDialog.sigCancel.connect(self.saveDialog.cancel)
        
        #Initialize dialog window for reloading data from file
        self.reloadDialog = MyReloadDialog(self)
        
        #Initalize timers for measurements
        self.timerplot = QtCore.QTimer()
        self.timerplot.timeout.connect(self.updateplot)
        
        self.timerguiupdate = QtCore.QTimer()
        self.timerguiupdate.timeout.connect(self.statemachine)
        # Single Shot timer for ongoing measurements
        self.timeracttemp = QtCore.QTimer()
        
        self.status = "starting"      
        #Variable for data aquisition
        self.lastx = 300
        self.datasize = 100000
        self.maxtime()
        self.x = [0]*self.datasize
        self.y = [0]*self.datasize

    def fileQuit(self):
        self.status = "quit"

    def closeEvent(self, ce):
        self.fileQuit()

    #File menu content
    def about(self):
        QtGui.QMessageBox.about(self, "About",
"""
Copyright Livia Waller LPC ETHZ 2015
Copyright Tiago Neves LPC ETHZ 2020

""")                                
        
    def contact(self):
        QtGui.QMessageBox.about(self, "Troubleshooting",
"""
- Is the ADC connected to PC?
- Did you choose the right channel?
- Did you choose the right measurement settings?
- Close program, disconnect USB, connect USB again and start program
More trouble with this program? Contact the engineer!
""")    
    
    #the State machine handles the flow of the program, controlled by timer "timerguiupdate"      
    def statemachine(self):
        #print("The Status is: ", self.status)
        #if unchanged the next state machine remains the same
        newstatus = self.status      
        if self.status == "starting":
            #Disable interface until it's ready
            self.dockWidget.setDisabled(True)
            self.dockWidget.statusmsg.setText("STARTING")
            #start new thread to get serial port number in background
            self.thread = Thread(target = self._lib.findcom)
            #self._lib.findcom()
            self.thread.start()
            newstatus= "connecting"

        elif self.status == "connecting":
            #verify that no thread is not running
            if self.thread.is_alive() == False:
                #If no serial port was found
                if self._lib.ser.port == '1000':
                    #Display error message
                    self.msg.setText('No devices found, please check connections and restart the application')
                    self.msg.exec()
                    #Close the Program
                    self.close()
                else:
                    self.dockWidget.statusmsg.setText("CONNECTING")
                    #Start new thread to configure arduino in background
                    self.thread = Thread(target = self.acttempstart)
                    self.thread.start()
                    newstatus = "ready"

        elif self.status == "ready":
            #verify that no thread is not running
            if self.thread.is_alive() == False:
                #self.acttemp()
                self.dockWidget.statusmsg.setText("READY")
                #Enable interface when ready
                self.dockWidget.setDisabled(False)
                newstatus= "readyloop"
                #trigger timer to do a loop measurement when ready
                self.timeracttemp.singleShot(50, self.acttemp)

        elif self.status == "readyloop":
            # verify that no thread is not running
            if self.thread.is_alive() == False:
                self.dockWidget.statusmsg.setText("READY")

        elif self.status == "start_measuring":
            #Disable interface until it's measuring
            self.dockWidget.setDisabled(True)
            #verify that no thread is not running
            if self.thread.is_alive() == False:
                self.dockWidget.statusmsg.setText("STARTING")
                self.setWindowTitle("UNSAVED <New Measurement>               Temperature Logger")
                #Stop measuring in between measurements
                self._lib.stop()
                #Start new thread to configure arduino in background
                self.thread = Thread(target = self.configmeasurement)
                self.thread.start()
                self.time = 0 
                self.timess = time.perf_counter()
                newstatus= "measuring"
                
        elif self.status == "measuring":
            #verify that no thread is not running
            if self.thread.is_alive() == False:
                #Disable interface until it's measuring
                self.dockWidget.setDisabled(False)
                #Start timer
                self.timerplot.start(0)
                newstatus= "loopmeasuring"

        elif self.status == "loopmeasuring":
            if self.thread.is_alive() == False:
                if (self.count % 2 == 0):
                    self.dockWidget.statusmsg.setText("MEASURING.")
                else:
                    self.dockWidget.statusmsg.setText("MEASURING...")

        elif self.status == "quit":
            #Stop timer
            self.timeracttemp.stop()
            self.timerguiupdate.stop()
            self.timerplot.stop()
            #Stop serial connection
            self._lib.stop()
            #close the program
            self.close()
            
        else:
            self.dockWidget.statusmsg.setText("UNKNOWN")
        #Update the state machine status
        self.status= newstatus
        #print("The NEW Status is: ", self.status)    
    
    #Function called with Start-Button clicked, starts measurement                          
    def start(self, *args):
        #setup state machine to start measuring
        self.status = "start_measuring"
        #uncheck all toolbar buttons      
        self.dockWidget.zoom.setAutoExclusive(False);
        self.dockWidget.zoom.setChecked(False);
        self.dockWidget.zoom.setAutoExclusive(True)
        self.dockWidget.cursor.setAutoExclusive(False);
        self.dockWidget.cursor.setChecked(False);
        self.dockWidget.cursor.setAutoExclusive(True)
        self.dockWidget.startbt2.setDisabled(True)
        self.dockWidget.intervall.setDisabled(True)
        self.dockWidget.scale.setDisabled(True)
        self.dockWidget.channel.setDisabled(True)
        self.dockWidget.toolbar.setDisabled(True)
        self.dockWidget.stopbt2.setDisabled(False)
        #Reset plot 
        self.myWidget.vb.removeItem(self.dockWidget.line1)
        self.myWidget.vb.removeItem(self.dockWidget.line2)
        self.myWidget.vb.removeItem(self.myWidget.label1)

    #Function called in a new thread to setup a new measurement 
    def configmeasurement(self):
        #reset data acquisiton arrays
        self.x = [0]*self.datasize
        self.y = [0]*self.datasize
        self.i = 0
        self._lib.xdata = []
        self._lib.wbold = 0.0
        self.count = 0
        #Evaluate which channel is chosen (left/right_)
        if self.dockWidget.l.isChecked() == True:
            self.channel = str(1) #left
        else: 
            self.channel = str(2) #right
        #Read measurement intervall
        self.intervall = self.dockWidget.intervallbox.value()
        #Calculate plot updating rate
        if self.intervall < 1.0:
            self.countplot = math.ceil(0.2/self.intervall)-1
        else:
            self.countplot = 0
        #Open serial connection to arduino
        self._lib.startcom(self.intervall,self.dockWidget.scalebox.currentText(), self.channel, self.datasize)
         
    #Function called with Stop-Button to stop measuring         
    def stop(self, *args):
        #Stop measurement timer
        self.timerplot.stop()
        self.dockWidget.startbt2.setDisabled(False)
        self.dockWidget.intervall.setDisabled(False)
        self.dockWidget.scale.setDisabled(False)
        self.dockWidget.channel.setDisabled(False)
        self.dockWidget.toolbar.setDisabled(False)
        self.dockWidget.zoom.setDisabled(False)
        self.dockWidget.cursor.setDisabled(False)
        self.dockWidget.savebt.setDisabled(False)
        self.dockWidget.stopbt2.setDisabled(True)
        self.timesss = time.perf_counter() - self.timess
        self.count = 0
        self.myWidget.vb.setRange(xRange =(self.x[0:self.i]),yRange=(min(self.y[:self.i]),max(self.y[:self.i]))) 
        self.myWidget.p1.setData(x = self.x[:self.i], y = self.y[:self.i], clear = True, connect = 'finite')
        #setup state machine to return to connecting after measurement routine is done
        self.status = "starting"
    
    #Function to add new data to the plot, controlled by measurement timer "timerplot"  
    def updateplot(self):      
        if self.i == self.datasize: 
            self.stop()
        else:     
            self._lib.updatadata()
            #Add new data point to the array
            self.x[self.i] = self.time
            self.y[self.i] = self._lib.ydata[self.i]
            #if time is smaller than 1s then plot not every new point, plot every second point
            if self.count == self.countplot:
                self.dockWidget.acttemp.clear() #Clear current temperature dispoly
                self.dockWidget.acttemp.insert(str('{:7.5f}'.format(self.y[self.i])))#update current temperature dispoly
                if self.i < self.lastx and self.i!= 0:
                    self.myWidget.p1.setData(x = self.x[0:self.i], y = self.y[0:self.i], clear = True, connect = 'finite')
                    self.myWidget.vb.setYRange(min(self.y[0:self.i]),max(self.y[0:self.i]),padding=0.1)
                    self.myWidget.vb.setXRange(0,self.x[self.i-1]) 
                elif self.i!= 0:
                    #adjust the displayed range of plot and add new data point
                    xxrange = self.x[(self.i-self.lastx):self.i]
                    yyrange = self.y[(self.i-self.lastx):self.i]
                    minx = min(self.x[(self.i-self.lastx):self.i])
                    maxy = max(self.y[(self.i-self.lastx):self.i])
                    miny = min(self.y[(self.i-self.lastx):self.i])
                    self.myWidget.p1.setData(x = xxrange, y = yyrange, clear = True, connect = 'finite')
                    self.myWidget.vb.setYRange(miny,maxy,padding=0.1)
                    self.myWidget.vb.setXRange(minx,self.x[self.i]) 
                self.count = 0
            else:
                self.count += 1
            self.time =self.time+self.intervall
            self.i += 1
            
    #Start periodic ADC acquisitions (0.2s) 
    def acttempstart(self):
        if self.dockWidget.l.isChecked() == True:
            channel = str(1)
        else: 
            channel = str(2)       
        self._lib.startcomacttemp(0.2,self.dockWidget.scalebox.currentText(), channel)

    #Get new data from Arduino while measuring in between data points
    def acttemp(self):
        if self.status == "readyloop":
            data = self._lib.acttemp()
            self.dockWidget.acttemp.clear()
            self.dockWidget.acttemp.insert(str('{:7.5f}'.format(data)))
            #Trigger single shot for next measurement
            self.timeracttemp.singleShot(150,self.acttemp)

    #Calculate maximal measurement time for desired timer intervall
    def maxtime(self):
        self.intervall = self.dockWidget.intervallbox.value()
        self.maxmtime = self.datasize*self.intervall
        s = self.maxmtime % 60
        m = (self.maxmtime-s)/60
        self.dockWidget.maxmtime.clear()
        self.dockWidget.maxmtime.insert(str(int(m))+'min  '+str(int(s))+'s')

    #Check/uncheck zoom button
    def zoomon(self):
        if self.myWidget.vb.zoom == 1:
           self.myWidget.vb.zoom = 0
        else:
            self.myWidget.vb.zoom = 1 
    
    #Zoom in on plot              
    def zoom(self, coords):  
        if self.dockWidget.zoom.isChecked() == True:  
            points = list(coords)
            self.myWidget.vb.setRange(xRange =(points[0],points[2]),yRange=(points[1],points[3]))
    
    #Undo zooming on range, reinstall original range of plot        
    def zoomhome(self):
        if self.dockWidget.zoom.isChecked() & self.dockWidget.toolbar.isEnabled()== True:
            self.myWidget.vb.setRange(xRange =(min(self.x[:self.i]),max(self.x[:self.i])),yRange=(min(self.y[:self.i]),max(self.y[:self.i])))

    #Channel selection
    def channel(self):
        #Stop Serial connection
        self._lib.stop()
        #setup state machine to start
        self.status = "starting"

    #Add the cursor lines
    def cursor(self):  
        if self.dockWidget.cursor.isChecked() & self.dockWidget.toolbar.isEnabled()== True:
            self.tree = np.asarray(self.x[0:self.i])
            self.dockWidget.line1.setBounds([min(self.x[0:self.i]),max(self.x[0:self.i])])
            self.dockWidget.line2.setBounds([min(self.x[0:self.i]),max(self.x[0:self.i])])
            self.dockWidget.line1.setValue(0)
            self.dockWidget.line2.setValue(0)
            self.myWidget.vb.addItem(self.dockWidget.line1)
            self.myWidget.vb.addItem(self.dockWidget.line2)
            self.myWidget.l.addItem(self.myWidget.label1, 0,1)
            self.dockWidget.line1.setPos(self.x[int(round(len(self.tree)*0.2,0))])
            self.dockWidget.line2.setPos(self.x[int(round(len(self.tree)*0.7,0))])
            self.cursormoved()
                
    #Move the cursor line with mouse drag and display current data point
    def cursormoved(self):
        self.index1 = (np.abs(self.tree - self.dockWidget.line1.value()).argmin())
        self.index2 = (np.abs(self.tree - self.dockWidget.line2.value()).argmin())
        self.dockWidget.line1.setPos(self.x[self.index1])
        self.dockWidget.line2.setPos(self.x[self.index2])
        self.xvalue1=str('{0:.2f}'.format(self.dockWidget.line1.value()))
        self.yvalue1=str('{0:.4f}'.format(self.y[self.index1]))
        self.xvalue2=str('{0:.2f}'.format(self.dockWidget.line2.value()))
        self.yvalue2=str('{0:.4f}'.format(self.y[self.index2]))
        self.dx=str('{0:.2f}'.format(self.dockWidget.line2.value()-self.dockWidget.line1.value()))
        self.dy=str('{0:.4f}'.format(self.y[self.index2]-self.y[self.index1]))
        self.myWidget.label1.setText('Cursor 1     t:     '+self.xvalue1+'    T:    '+self.yvalue1+'     Cursor 2  t:     '+self.xvalue2+'    T:     '+self.yvalue2+'                   dt:     '+self.dx+'     dT:     '+self.dy)      
    
    #Call the save window
    def save(self):
        self.saveDialog.execute()
    
    #Send data to save tp file    
    def savedata(self):
        if self.dockWidget.l.isChecked() == True:
            self.channel = str(1)
        else: 
            self.channel = str(2)
        #self.saveDialog.savedata(self.dockWidget.intervallbox.value(),self.dockWidget.scalebox.currentText(), self.channel, self.x, self._lib.ydata, self._lib.ydatastr)
        self.saveDialog.savedata(self.dockWidget.intervallbox.value(),self.dockWidget.scalebox.currentText(), self.channel, self.x, self.y, self.i)
        self.setWindowTitle("SAVED:"+self.saveDialog.filename+"   Temperature Logger")

    #Reload data from file     
    def reload(self):
        self.newdatax=[]
        self.newdatay=[]
        if self.reloadDialog.execute() == True:
            reload_path= os.path.abspath(self.reloadDialog.fname[0])
        else:
            return
        data = []
        with open(reload_path, 'r') as f:
            for l in f:
               data.append(l)
        #find Timestep, Channel und Scale Factor
        indts = [i for i ,s in enumerate(data) if "##Time step  " in s]
        indch = [i for i ,s in enumerate(data) if "##Channel  " in s]
        indsc = [i for i ,s in enumerate(data) if "##Scale Factor  " in s]
        ts = data[indts[0]][13:]
        ch = data[indch[0]][11:]
        ch=ch.rstrip()
        sc = data[indsc[0]][16:]
        #set the values
        self.dockWidget.intervallbox.setValue(float(ts))
        if ch == "Left":
            self.dockWidget.l.setChecked(True)
        else:
            self.dockWidget.r.setChecked(True)
        if sc == "200":
            self.dockWidget.scalebox.setCurrentIndex(2)
        elif sc == "100":
            self.dockWidget.scalebox.setCurrentIndex(1)
        else:
            self.dockWidget.scalebox.setCurrentIndex(0)
        #delete all elements with ##
        inddel = [i for i ,s in enumerate(data) if "##" in s]
        for b in inddel:  
            del data[0]
        #split dataelements 
        self.i = 0
        for i, elem in enumerate(data): 
            nd = data[i].split("  ")
            nd[0]= float(nd[0].rstrip())
            if nd[1] == '':
                nd[1] = np.nan
            else:
                nd[1]= float(nd[1].rstrip())
            self.x[self.i] = nd[0]
            self.y[self.i] = nd[1]
            self.i += 1
        self.setWindowTitle("FILE OPENED:" + reload_path + "   Temperature Logger")
        self.reloadplot()  

    #Replot data from file to plot    
    def reloadplot(self):     
        self.myWidget.p1.setData(x = self.x[:self.i], y = self.y[:self.i], clear = True)
        self.myWidget.vb.setYRange(min(self.y[:self.i]),max(self.y[:self.i]),padding=0.1)
        self.myWidget.vb.setXRange(min(self.x[:self.i]),max(self.x[:self.i])) 
        self.dockWidget.zoom.setDisabled(False)
        self.dockWidget.cursor.setDisabled(False)
        self.dockWidget.savebt.setDisabled(False)
        self.myWidget.vb.removeItem(self.dockWidget.line1)
        self.myWidget.vb.removeItem(self.dockWidget.line2)
        self.cursor()     

#Class for the Widget in the Mainframe                
class MyWidget(QtGui.QFrame, Ui_MyWidget):

    def __init__(self, parent=None):
        super(MyWidget, self).__init__(parent)
        self.setupUi(self)
        
#Sidebar widget, part of MyWidget
class MyDockWidget(QtGui.QDockWidget, Ui_MyDockWidget):
    #Emitted signals of sidebar
    sigStart = QtCore.pyqtSignal()  
    sigStop = QtCore.pyqtSignal() 
    sigSave = QtCore.pyqtSignal() 
    sigCursor = QtCore.pyqtSignal() 
    sigZoom = QtCore.pyqtSignal() 
    sigReload = QtCore.pyqtSignal()
    sigChannel = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(MyDockWidget, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Setting')
        #Connect sidebar events to signals
        self.startbt2.clicked.connect(self.sigStart)
        self.stopbt2.clicked.connect(self.sigStop)
        self.savebt.clicked.connect(self.sigSave)
        self.cursor.clicked.connect(self.sigCursor)
        self.zoom.toggled.connect(self.sigZoom)
        self.reloadbt.clicked.connect(self.sigReload)
        self.l.toggled.connect(self.sigChannel)
        #self.r.toggled.connect(self.sigChannel)

#Window to save data to file
class MySaveDialog(QtGui.QDialog, Ui_Save):        
    sigOk = QtCore.pyqtSignal()
    sigCancel = QtCore.pyqtSignal()
    
    def __init__(self, parent=None):
        super(MySaveDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Save File')
        self.okbt.clicked.connect(self.sigOk)
        self.cancelbt.clicked.connect(self.sigCancel)

    def execute(self):
        self.exec_()
        
    #actual saving of data to file, format data for file, add name etc. from save window to file
    def savedata(self,intervall, scale, channel, xdata, ydata, arrlength):
        name = self.line1.text()+'.dat'
        #save_path = os.path.abspath(r'C:\Users\Public') #Mein PC
        #save_path = os.path.abspath(r'C:\Users\pcp\Documents\T_Logger_Data_L') #TreVac
        #save_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #KAL
        #save_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #VBW
        save_path = os.path.realpath(os.path.expanduser('~/Desktop/LoggerData'))
        pathlib.Path(save_path).mkdir(parents=True,exist_ok=True) #Make directory if does not exist
        self.filename = os.path.join(save_path,name)
        if os.path.isfile(self.filename) == True:
            f = QtGui.QFont()
            f.setBold(True)
            d = QtGui.QDialog()
            d.setWindowTitle("Warning")
            d.resize(200,50)
            ly = QtGui.QVBoxLayout()
            lb = QtGui.QLabel('File with this name already exists!')
            lb.setFont(f)
            ly.addWidget(lb)
            d.setLayout(ly)
            d.exec()
        else:
            file = open(self.filename, "w")
            file.write('##TITLE  '+self.line1.text()+'\n')
            file.write('##NAME  '+self.line2.text()+'\n')
            file.write('##SAMPLE  '+self.line3.text()+'\n')
            file.write('##SOLVENT  '+self.line4.text()+'\n')
            file.write('##COMMENT  ')              
            lines= self.line5.toPlainText().split('\n')
            for i in range(len(lines)):
                if i == 0:
                    file.write(lines[i]+'\n')
                else:
                    file.write('##'+lines[i]+'\n')
            if channel == '1':
                file.write('##Channel  '+'Left' +'\n') #Left/right
            if channel == '2': 
                file.write('##Channel  '+'Right' +'\n') #Left/right
            file.write('##Time step  '+str(intervall)+'\n')
            file.write('##Scale Factor  '+str(scale)+'\n')
            file.write('##'+'x-data'+'  '+'y-data'+'\n')
            for v in range(arrlength):
                if ydata[v] == np.nan:
                    file.write(str("{:5.3f}".format(xdata[v]))+'  '+'nan'+'\n')
                else:
                    file.write(str("{:5.3f}".format(xdata[v]))+'  '+str("{:7.5f}".format(ydata[v]))+'\n')
                 #file.write(str("{:5.3f}".format(xdata[v]))+'  '+ydata[v]+'  '+str(ydatastr[v])+'\n')
            file.close()        
            self.close()
        self.line1.clear()
        self.line2.clear()
        self.line3.clear()
        self.line4.clear()
        self.line5.clear()        
        
    def cancel(self):
        self.close()  
        self.isExecuted = 0

#PopUp Window for reloading older data into plot
class MyReloadDialog(QtGui.QFileDialog):

    def __init__(self, parent=None):
        super(MyReloadDialog, self).__init__(parent)
        #reload_path = os.path.abspath(r'C:\Users\Public') #Mein PC
        #reload_path = os.path.abspath(r'C:\Users\pcp\Documents\T_Logger_Data_L') #TreVac
        #reload_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #KAL
        #reload_path = os.path.abspath(r'C:\Users\Student\Desktop\TempLoggerData') #VBW
        reload_path = os.path.realpath(os.path.expanduser('~/Desktop/LoggerData'))
        pathlib.Path(reload_path).mkdir(parents=True,exist_ok=True) #Make directory if does not exist
        self.setDirectory(reload_path)
        self.setNameFilter("*.dat")
        #self.setFilter("*.dat")
        self.setFileMode(self.ExistingFile)

    def execute(self):
        #self.exec_()
        if self.exec_() == self.Accepted:
            self.fname = self.selectedFiles()
            return True
        else:
            return False

#Main
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv[1:])
    view = Main()
    view.show()
    #stup state machine to start
    view.status = "starting"
    #print("type is:",type(sys.argv[1]))
    if len(sys.argv) == 2:  #Number of command line parameters (Nr1 is the program name )
        try:    #Check if the str value can turn into a float number
                view.dockWidget.intervallbox.setValue(float(sys.argv[1]))
                print("Valid Input parameter", float(sys.argv[1]))
        except ValueError:
                print("Invalid Input parameter", sys.argv[1])
                print("Default value used")
                #Default interval of measuremnts
                #view.dockWidget.intervallbox.setValue(0.2) #TreVac
                view.dockWidget.intervallbox.setValue(0.5) #KAL
                #view.dockWidget.intervallbox.setValue(1) #VBW
    #start timer to update the GUI
    view.timerguiupdate.start(100)
    sys.exit(app.exec_())
