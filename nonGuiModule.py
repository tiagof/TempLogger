# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 07:36:28 2016

@author: wallerl & tiagof
Library class for non-gui routines:
- find COM-Port of Arduino
- start/stop communication with Arduino for measurements
- receive data from Arduino while measurement
- start/stop communcation with Arduino in between measurements
- receive data from Arduino in between measurements
"""

import serial
import time
import serial.tools.list_ports
import numpy as np
from PyQt5 import QtCore, QtGui

#Library class for all communincation with Arduino
class LibModule(object):
	
	def __init__(self, parent=None):
		self.ser = serial.Serial()
		self.ser.port== 1000 #default number, not an actual port
		self.ser.baudrate = 57600 
		self.ydata = []
		self.ser.timeout = 10.0

	#Com with Arduino for measurements
	#setup Com with Arduino
	def startcom(self, intervall, scale, channel, datasize):
		self.intervall = str('{:03}'.format(intervall))
		self.k = 0
		self.ydata = [0]*datasize #wipe ydata array 
		self.ser.open() 
		# self.ser.flushInput()
		# time.sleep(0.1)
		# self.ser.flushOutput()
		# time.sleep(.1)
		self.ser.write(str.encode('ss20')) #set Sampling rate
		print(self.ser.readline())
		#time.sleep(.1)
		#set channel, tmperature scale
		if scale == '50':
			self.ser.write(str.encode('sc'+channel+',0,4'))
			print('sc'+channel+',0,4')
		elif scale == '25':
			self.ser.write(str.encode('sc'+channel+',0,8'))
		elif scale == '100':
			self.ser.write(str.encode('sc'+channel+',0,2')) 
		elif scale == '200':
			self.ser.write(str.encode('sc'+channel+',0,1'))
		else:
			pass
		print(self.ser.readline())
		self.ser.write(str.encode('si'+self.intervall)) #set measure intervall
		print(self.ser.readline())
		#start continuous measurement
		self.ser.write(str.encode('gcv'))
		print(self.ser.readline())
		print("Serial Com Port is starting")

	#receive new data while measurement
	#check on validity of data, correct format
	#normal: format is correct
	#invalid formats:
	#no float: zero and decimal points are missing
	#length: there are not enough digits 
	#protocol: protocol is not right/complete
	def updatadata(self):
		#print('Reading Data from Arduino')
		wb=self.ser.readline()
		print('Data= ', wb)
		if  wb.endswith(b'\r\n'):
			wb = str(wb)
			wb = wb[:-5] #Remove the last 5 Characters "\r\n'"
			wb = wb[2:]  #Remove the first 2 Characters "b'"
			if len(wb) == 11: #Check if the lenght is 11 Characters EX:1.123456789
				try:
					wb=(float(wb))
					if (wb > float(2.048)) or (wb < float((-2.048))):  #Check if the number is in the Range
						self.ydata[self.k] = np.nan
					else:
						#print('normal')
						wb=float(wb*100)
						self.ydata[self.k] = wb 
				except ValueError:
					#print('no float')
					self.ydata[self.k] = np.nan
			else:
				#print('length')
				self.ydata[self.k] = np.nan
		else:
			#print('protocol')
			self.ydata[self.k] = np.nan
		self.k += 1
	 
	#stop receiving data from Arduino, flush buffer and close Com 
	def stop(self):
		if(self.ser.isOpen()==True):
			self.ser.write(str.encode('s'))
			self.ser.flushInput()
			time.sleep(.1)
			self.ser.flushOutput()
			time.sleep(.1)
			self.ser.close()
			print("Serial Com Port is stopping")

  	#Com with Arduino in between measurement, measurement intervall + 1s
	#setup Com with Arduino
	def startcomacttemp(self, intervall, scale, channel):
		self.intervall = intervall
		self.intervall = str('{0:.2f}'.format(self.intervall))
		if(self.ser.isOpen()==False):
			self.ser.open() 
		else:
			self.ser.close()
			time.sleep(1.0)
			self.ser.open() 
		self.ser.flushInput()
		time.sleep(0.1)
		self.ser.flushOutput()
		time.sleep(.1)
		self.ser.write(str.encode('ss20')) #set Sampling rate
		print(self.ser.readline())
		time.sleep(.1)
		if scale == '50':
			self.ser.write(str.encode('sc'+channel+',0,4')) 
			print('sc'+channel+',0,4')
		elif scale == '25':
			self.ser.write(str.encode('sc'+channel+',0,8'))
		elif scale == '100':
			self.ser.write(str.encode('sc'+channel+',0,2')) 
		elif scale == '200':
			self.ser.write(str.encode('sc'+channel+',0,1'))
		else:
			pass
		print(self.ser.readline())
		self.ser.write(str.encode('si'+self.intervall)) #set measure intervall
		print(self.ser.readline())
		#start continuous measurement
		self.ser.write(str.encode('gcv'))
		print(self.ser.readline())

	#receive new data between measurements
	def acttemp(self):
		adata= self.ser.readline()
		adata = str(adata)
		adata = adata[:-5] #Remove the last 5 Characters "\r\n'"
		adata = adata[2:]  #Remove the first 2 Characters "b'"
		adata=(float(adata))
		if (adata > 2.048) or (adata < (-2.048)): #Check if the number is not in the Range
			adata = np.nan
		else:
			adata=adata*100  #makes volts*100 EX:0.2V*100
			adata=float("{:7.5f}".format((adata))) #round to 5 decimal
		return(adata)

#Automatically find port number of Arduino
#Cant start application incase no port is found
	def findcom(self):
		self.ser.port = '1000'
		ports = []
		print("Scan Serial Ports")
		ports = list(serial.tools.list_ports.comports())
		#ports= serial.tools.list_ports.comports()
		for p in ports:
			print(p.device)
			print(p.manufacturer)
			print(p.name)
			print(p.serial_number)
			print(p.location)
			print(p.description)
			print(p.product)
			print(p.interface)

		print(len(ports), 'ports found')

		if not ports:
			print("No devices on ports found, please restart application") 

		#scan ports for Ardunio
		#for a in range(len(ports)):
		for port in ports:
			if 'Arduino' in str(port.manufacturer): #Linux adaptation
			#if 'Arduino Uno' in str(ports[a]):
				print("Arduino Ports found")
				self.ser.port= port.device
				self.ser.baudrate = 57600
				self.ser.open()
				answer = self.ser.readline()
				print(answer)
				if answer == b'Ardu_24BitADC r3\r\n':
					print("Arduino Connection established")
					self.ser.close()
					break
		if self.ser.port == 1000:
			self.ser.close()

#Automatically find port number of Arduino
#Cant start application incase no port is found
	def findcom2(self):
		self.ser.port = '1000'
		ports = []
		print("Scan Serial Ports")
		ports = list(serial.tools.list_ports.comports())
		#ports= serial.tools.list_ports.comports()
		for p in ports:
			print(p.device)
			print(p.manufacturer)
			print(p.name)
			print(p.serial_number)
			print(p.location)
			print(p.description)
			print(p.product)
			print(p.interface)

		print(len(ports), 'ports found')

		if not ports:
			print("No devices on ports found, please restart application") 

		#scan ports for Ardunio
		#for a in range(len(ports)):
		for port in ports:
			if 'Arduino' in str(port.manufacturer): #Linux adaptation
			#if 'Arduino Uno' in str(ports[a]):
				print("Arduino Ports found")
				self.ser.port= port.device
				self.ser.baudrate = 57600
				self.ser.open()
				answer = self.ser.readline()
				print(answer)
				if answer == b'Ardu_24BitADC r3\r\n':
					print("Arduino Connection established")
					#self.ser.close()
					break
		if self.ser.port == 1000:
			self.ser.close()

	#Com with Arduino for measurements
	#setup Com with Arduino
	def startcom2(self, intervall, gain, channel, datasize):
		#self.intervall = str('{:03}'.format(intervall))
		self.k = 0
		self.ydata = [0]*datasize #wipe ydata array
		if self.startcomacttemp2(intervall, gain, channel):
			return True
		else:
			return False

	def startcomacttemp2(self, intervall, gain, channel):
		self.gain = str(gain)
		self.intervall = str('{0:.2f}'.format(intervall))
		# set Sampling rate, it also stops any measurements running
		self.ser.write(str.encode('ss20'))
		print(self.ser.readline())
		time.sleep(.1)
		self.ser.flushOutput()
		self.ser.flushInput()
		time.sleep(.5)
		self.ser.write(str.encode('ss20'))#set Sampling rate
		if self.ser.readline() == b'T\r\n':
			print("Ready to send new commands")
			self.ser.write(str.encode('sc'+channel+',0,'+self.gain))
			print('sc'+channel+',0,'+self.gain)
			print(self.ser.readline())
			self.ser.write(str.encode('si'+self.intervall)) #set measure intervall
			print(self.ser.readline())
			#Set IDAC Current to 50uA
			#self.ser.write(str.encode('sr9,10'))
			# Set IDAC Current to 250uA
			self.ser.write(str.encode('sr11,10'))
			print(self.ser.readline())
			#Map IDAC exitation current to AN1
			self.ser.write(str.encode('sr16,11'))
			print(self.ser.readline())
			# start continuous measurement
			self.ser.write(str.encode('gcv'))
			print(self.ser.readline())
			return True
		else:
			print("!!NOT READY to send new commands!!")
			return False
